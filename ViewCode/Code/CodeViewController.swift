//
//  CodeViewController.swift
//  ViewCode
//
//  Created by Jonathan Martins on 16/07/19.
//  Copyright © 2019 Jonathan Martins. All rights reserved.
//

import UIKit
import Foundation

class CodeViewController: UIViewController {
    
    // MARK: - Variables
    private unowned var codeView:CodeView { return self.view as! CodeView }
    
    // MARK: - LifeCycle
    override func loadView() {
        self.view = CodeView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        codeView.bind(to: self)
    }
    
    deinit {
        print("CodeViewController deallocated")
    }
}

// MARK: - General Functions
extension CodeViewController{
    
    /// Shows an alert with title and message
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
