//
//  CodeView.swift
//  ViewCode
//
//  Created by Jonathan Martins on 16/07/19.
//  Copyright © 2019 Jonathan Martins. All rights reserved.
//

import UIKit
import Foundation

class CodeView:UIView{
    
    // MARK: - Variables
    /// The ViewController owner of this view
    private weak var controller:CodeViewController?
    
    /// The view's title
    private var title:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .center
        label.text = "Alerta"
        label.font = UIFont.systemFont(ofSize: 18, weight: .medium)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /// The view's message
    private var message:UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.textColor = .black
        label.textAlignment = .center
        label.text = "Mensagem"
        label.font = UIFont.systemFont(ofSize: 16, weight: .light)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    /// The view's button
    private var button:UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.setTitle("Confirmar", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    /// The view's button
    private var button2:UIButton = {
        let button = UIButton()
        button.backgroundColor = .red
        button.setTitle("Confirmar", for: .normal)
        button.titleLabel?.font = UIFont.systemFont(ofSize: 14, weight: .regular)
        button.translatesAutoresizingMaskIntoConstraints = false
        return button
    }()
    
    /// The view's wrapper
    private var wrapper:UIView = {
        let wrapper = UIView()
        wrapper.clipsToBounds = true
        wrapper.layer.cornerRadius = 7
        wrapper.layer.borderWidth  = 0.5
        wrapper.backgroundColor    = .white
        wrapper.layer.borderColor  = UIColor.lightGray.cgColor
        wrapper.translatesAutoresizingMaskIntoConstraints = false
        return wrapper
    }()
    
    // MARK: - LifeCycle
    override init(frame: CGRect) {
        super.init(frame: frame)
        addViews()
        setupConstraints()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addViews()
        setupConstraints()
    }
}

// MARK: - Setup
extension CodeView{
    
    /// Binds the given Controller to this View
    func bind(to controller:CodeViewController){
        self.controller = controller
        button.addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    /// Adds the subviews to the main view
    private func addViews(){
        self.backgroundColor = .white
        self.addSubview(wrapper)
        wrapper.addSubview(title)
        wrapper.addSubview(message)
        wrapper.addSubview(button)
    }
    
    /// Sets up the constraints on the views
    private func setupConstraints(){
        NSLayoutConstraint.activate([
            wrapper.centerXAnchor.constraint(equalTo: self.centerXAnchor),
            wrapper.centerYAnchor.constraint(equalTo: self.centerYAnchor),
            wrapper.widthAnchor  .constraint(equalTo: self.widthAnchor   , multiplier: 1/1.2),
            
            title.topAnchor    .constraint(equalTo: wrapper.topAnchor     , constant: 20),
            title.widthAnchor  .constraint(equalTo: wrapper.widthAnchor   , multiplier: 1/1.2),
            title.centerXAnchor.constraint(equalTo: wrapper.centerXAnchor),
            
            message.topAnchor     .constraint(equalTo: title.bottomAnchor   , constant: 10),
            message.leadingAnchor .constraint(equalTo: title.leadingAnchor ),
            message.trailingAnchor.constraint(equalTo: title.trailingAnchor),
            
            button.topAnchor     .constraint(equalTo: message.bottomAnchor   , constant: 50),
            button.bottomAnchor  .constraint(equalTo: wrapper.bottomAnchor),
            button.leadingAnchor .constraint(equalTo: wrapper.leadingAnchor ),
            button.trailingAnchor.constraint(equalTo: wrapper.trailingAnchor),
            button.heightAnchor  .constraint(equalToConstant: 60)
        ])
    }
}

// MARK: - Actions
extension CodeView{
    
    /// The click action of the button
    @objc private func buttonAction(_ sender:UIButton){
        controller?.showAlert(title:"ViewCode", message:"View criada por código")
    }
}
