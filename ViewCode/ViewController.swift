//
//  ViewController.swift
//  ViewCode
//
//  Created by Jonathan Martins on 17/07/19.
//  Copyright © 2019 Jonathan Martins. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    /// Opens the XibViewController
    @IBAction func didTapOpenXib(_ sender: Any) {
        navigationController?.pushViewController(XibViewController(), animated: true)
    }
    
    /// Opens the CodeViewController
    @IBAction func didTapOpenCode(_ sender: Any) {
        navigationController?.pushViewController(CodeViewController(), animated: true)
    }
}
