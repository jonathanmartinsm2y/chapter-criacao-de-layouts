//
//  XibViewController.swift
//  ViewCode
//
//  Created by Jonathan Martins on 16/07/19.
//  Copyright © 2019 Jonathan Martins. All rights reserved.
//

import UIKit

class XibViewController: UIViewController {
    
    @IBOutlet weak var titleLabel:   UILabel!
    @IBOutlet weak var wrapperView:  UIView!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var actionButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    
    /// Initial configuration of the views
    private func setupViews(){
        wrapperView.layer.cornerRadius = 7
        wrapperView.layer.borderWidth  = 0.5
        wrapperView.layer.borderColor  = UIColor.lightGray.cgColor
    }
}

// MARK: - Actions
extension XibViewController{
    
    @IBAction func didTapActionButton(_ sender: Any) {
        showAlert(title: "XIB", message: "View criada por interface gráfica.")
    }
}

// MARK: - General Functions
extension XibViewController{
    
    /// Shows an alert with title and message
    func showAlert(title:String, message:String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: nil))
        present(alert, animated: true, completion: nil)
    }
}
